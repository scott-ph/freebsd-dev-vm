#!/bin/sh
#-
# Copyright (c) 2019 Intel Corporation
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# $FreeBSD$

set -e

quote() {
	local prog
	prog=$(cat <<-'EOF'
		BEGIN {
			for (i = 1; i < ARGC; i++) {
				gsub(/'/,"'\\''",ARGV[i]);
				print "'" ARGV[i] "'";
			}
		}
	EOF
	)
	awk -v ORS=' ' "${prog}" "$@"
}

SCRIPT_ARGS=$(quote "$@")
ensure_root() {
	local sudo_path

	if [ "$(id -u)" -eq 0 ]; then
		return 0
	fi
	sudo_path=$(command -v sudo 2>&- || :)
	if [ -n "${sudo_path}" ]; then
		eval "exec sudo -p 'sudo Password: ' env HOME=${HOME} sh \"$0\" ${SCRIPT_ARGS}"
	fi
	printf 'su root '
	exec su root "sh \"$0\" ${SCRIPT_ARGS}"
}

EXIT_HANDLER=
trap 'eval " set +e; ${EXIT_HANDLER}"' EXIT

atexit() {
	EXIT_HANDLER=" $(quote "$@");${EXIT_HANDLER}"
}

run() {
	local confdir destdir tap

	if ! [ -x "$(which dnsmasq 2>&-)" ]; then
		echo dnsmasq required >&2
		return 1
	fi
	ensure_root

	kldload -n if_tuntap >&- 2>&- || :
	kldload -n vmm >&- 2>&- || :

	confdir=$(mktemp -d)
	atexit rm -rf ${confdir}

	tap=$(ifconfig tap create inet 192.168.8.1 netmask 255.255.255.252 up)
	atexit ifconfig ${tap} destroy

	dnsmasq -C /dev/null -i ${tap} -k -l ${confdir}/dnsmasq.leases \
	    -F 192.168.8.2,192.168.8.2,255.255.255.252 \
	    -M "${DESTDIR}/boot/loader.efi" \
	    -O 17,nfs://192.168.8.1/"${DESTDIR}" --enable-tftp &
	atexit kill $!

	if ! grep -q "^${DESTDIR} " /etc/exports; then
		echo ${DESTDIR} -alldirs -maproot root -public \
		    -network 192.168.8.1 -mask 255.255.255.252 \
		    | tee -a /etc/exports > /dev/null
		atexit sed -i '' "\\@^${DESTDIR} @d" /etc/exports
	fi
	if pgrep -q nfsd; then
		service mountd forcerestart
	else
		service nfsd forcestart
		atexit service rpcbind forcestop
		atexit service mountd forcestop
		atexit service nfsd forcestop
	fi

	bhyve -c 2 -m 512m -H -s 0,hostbridge -s 1,virtio-net,${tap} \
	    -s 31,lpc -l com1,stdio \
	    -l bootrom,/usr/local/share/uefi-firmware/BHYVE_UEFI.fd -G 2345 vm
	atexit bhyvectl --vm=vm --destroy
}

do_gdb() {
	local gdb_cmds
	gdb_cmds=$(mktemp)
	atexit rm ${gdb_cmds}
	cat <<-EOF > ${gdb_cmds}
	set architecture i386:x86-64
	add-symbol-file ${DESTDIR}/usr/lib/debug/boot/kernel/kernel.debug
	target remote 127.0.0.1:2345
	EOF

	gdb --command=${gdb_cmds}
}

usage() {
	echo "usage: $0 [run|gdb] [destdir]"
}

DESTDIR="$2"
: ${DESTDIR:=$PWD}
DESTDIR=$(readlink -f "${DESTDIR}")
case "$1" in
run)	run;;
gdb)	do_gdb;;
*)	usage;;
esac
exit $?
